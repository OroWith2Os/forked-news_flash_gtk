using Gtk 4.0;
using Adw 1;

template $DiscoverDialog : Adw.Window {
  width-request: 360;
  height-request: 450;
  modal: true;
  default-width: 500;
  default-height: 500;

  Box {
    orientation: vertical;

    Adw.HeaderBar {
      valign: center;
      hexpand: true;
      vexpand: false;
      title-widget: Adw.WindowTitle {
        title: _("Discover");
        subtitle: "Search the feedly.com library";
      };
      show-start-title-buttons: true;
      show-end-title-buttons: true;

      styles [
        "flat",
      ]
    }

    Box {
      vexpand: true;
      orientation: vertical;
      spacing: 2;

      ScrolledWindow {
        vexpand: true;

        Adw.Clamp {
          margin-top: 5;
          margin-bottom: 5;
          maximum-size: 750;

          Box {
            margin-start: 10;
            margin-end: 10;
            margin-top: 10;
            margin-bottom: 10;
            orientation: vertical;
            spacing: 10;

            Box {
              spacing: 5;

              SearchEntry search_entry {
                hexpand: true;
                placeholder-text: _("Search by #topic, Website or RSS Link");
              }

              ComboBoxText language_combo {
                active: 0;
                items [
                  en_EN: "English",
                  de_DE: "German",
                ]
              }
            }

            Stack search_page_stack {
              hhomogeneous: false;
              vhomogeneous: false;
              transition-duration: 50;
              transition-type: crossfade;

              StackPage {
                name: "featured";
                title: _("page0");
                child: Box {
                  orientation: vertical;

                  Label {
                    focusable: false;
                    halign: start;
                    margin-top: 10;
                    margin-bottom: 10;
                    label: _("Featured Topics");

                    styles [
                      "heading",
                    ]
                  }

                  FlowBox {
                    homogeneous: true;
                    column-spacing: 5;
                    row-spacing: 5;
                    min-children-per-line: 1;
                    max-children-per-line: 2;
                    selection-mode: none;
                    activate-on-single-click: false;

                    FlowBoxChild news_card {
                      width-request: 150;
                      height-request: 50;
                      child: 
                      Button news_card_button {
                        label: _("#News");
                        focus-on-click: false;
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      }

                      ;
                    }

                    FlowBoxChild tech_card {
                      child: Button tech_card_button {
                        label: _("#Tech");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }

                    FlowBoxChild science_card {
                      child: Button science_card_button {
                        label: _("#Science");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }

                    FlowBoxChild culture_card {
                      child: Button culture_card_button {
                        label: _("#Culture");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }

                    FlowBoxChild media_card {
                      width-request: 100;
                      height-request: 80;
                      child: Button media_card_button {
                        label: _("#Media");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }

                    FlowBoxChild sports_card {
                      width-request: 100;
                      height-request: 80;
                      child: Button sports_card_button {
                        label: _("#Sports");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }

                    FlowBoxChild food_card {
                      width-request: 100;
                      height-request: 80;
                      child: Button food_card_button {
                        label: _("#Food");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }

                    FlowBoxChild foss_card {
                      width-request: 100;
                      height-request: 80;
                      child: Button foss_card_button {
                        label: _("#Open source");
                        receives-default: true;

                        styles [
                          "card",
                        ]
                      };
                    }
                  }
                };
              }

              StackPage {
                name: "search";
                title: _("page1");
                child: Box {
                  margin-top: 10;
                  orientation: vertical;
                  spacing: 10;

                  Label {
                    focusable: false;
                    halign: start;
                    margin-top: 10;
                    margin-bottom: 10;
                    label: _("Search Results");

                    styles [
                      "heading",
                    ]
                  }

                  Stack search_result_stack {
                    vexpand: true;
                    transition-duration: 50;
                    transition-type: crossfade;

                    StackPage {
                      name: "list";
                      child: 
                      Box {
                        orientation: vertical;

                        ListBox search_result_list {
                          height-request: 50;
                          selection-mode: none;

                          styles [
                            "boxed-list",
                          ]
                        }
                      }

                      ;
                    }

                    StackPage {
                      name: "spinner";
                      child: Box {
                        orientation: vertical;

                        Box {
                          height-request: 300;

                          Spinner {
                            focusable: false;
                            spinning: true;
                            hexpand: true;
                            vexpand: true;
                            halign: center;
                            valign: center;
                            width-request: 64;
                            height-request: 64;
                          }

                          styles [
                            "card",
                          ]
                        }
                      };
                    }

                    StackPage {
                      name: "empty";
                      child: Box {
                        orientation: vertical;

                        Label {
                          height-request: 300;
                          focusable: false;
                          vexpand: true;
                          label: _("No Results");

                          styles [
                            "card",
                            "large-title",
                          ]
                        }
                      };
                    }
                  }
                };
              }
            }
          }
        }
      }
    }
  }
}
