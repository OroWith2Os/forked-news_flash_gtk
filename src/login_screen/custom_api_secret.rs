use super::LoginPrevPage;
use crate::util::GtkUtil;
use glib::{clone, subclass, SignalHandlerId};
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, Label};
use libadwaita::EntryRow;
use news_flash::models::{ApiSecret, LoginGUI, PluginInfo};
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/custom_api_secret_page.ui")]
    pub struct CustomApiSecret {
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub info_text: TemplateChild<Label>,
        #[template_child]
        pub client_id_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub client_secret_entry: TemplateChild<EntryRow>,
        #[template_child]
        pub submit_button: TemplateChild<Button>,
        #[template_child]
        pub ignore_button: TemplateChild<Button>,

        pub client_id_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub client_secret_entry_signal: RefCell<Option<SignalHandlerId>>,
        pub api_secret: RefCell<Option<ApiSecret>>,
        pub prev_page: RefCell<Option<LoginPrevPage>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CustomApiSecret {
        const NAME: &'static str = "CustomApiSecret";
        type ParentType = gtk4::Box;
        type Type = super::CustomApiSecret;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for CustomApiSecret {}

    impl WidgetImpl for CustomApiSecret {}

    impl BoxImpl for CustomApiSecret {}
}

glib::wrapper! {
    pub struct CustomApiSecret(ObjectSubclass<imp::CustomApiSecret>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for CustomApiSecret {
    fn default() -> Self {
        Self::new()
    }
}

impl CustomApiSecret {
    pub fn new() -> Self {
        glib::Object::new::<Self>()
    }

    pub fn init(&self) {
        self.imp()
            .client_id_entry_signal
            .replace(Some(self.setup_entry(&self.imp().client_id_entry)));
        self.imp()
            .client_secret_entry_signal
            .replace(Some(self.setup_entry(&self.imp().client_secret_entry)));
    }

    pub fn fill(&self, secret: &ApiSecret) {
        self.imp().client_id_entry.set_text(&secret.client_id);
        self.imp().client_secret_entry.set_text(&secret.client_secret);
    }

    pub fn set_service(&self, info: &PluginInfo, prev_page: LoginPrevPage) {
        self.imp().prev_page.replace(Some(prev_page));
        if let LoginGUI::OAuth(oauth_info) = &info.login_gui {
            if let Some(create_secret_url) = &oauth_info.create_secret_url {
                self.imp()
                    .info_text
                    .set_markup(
                        &format!(
                            r#"This service uses API secrets to limit the amount of actions per day for every user of the app. You can generate your own secrets <a href="{}">here</a>. This way you don't have to share the limited amount of actions with other users of NewsFlash."#,
                            create_secret_url.as_str()
                        )
                    );
            }
        }
    }

    pub fn reset(&self) {
        self.imp().client_id_entry.set_text("");
        self.imp().client_secret_entry.set_text("");
    }

    fn setup_entry(&self, entry: &EntryRow) -> SignalHandlerId {
        entry.connect_text_notify(clone!(
            @weak self as page => @default-panic, move |_entry|
        {
            let complete = !GtkUtil::is_entry_row_emty(&page.imp().client_id_entry)
                && !GtkUtil::is_entry_row_emty(&page.imp().client_secret_entry);
            page.imp().submit_button.set_sensitive(complete);

            if complete {
                page.imp().api_secret.replace(Some(ApiSecret {
                    client_id: page.imp().client_id_entry.text().as_str().into(),
                    client_secret: page.imp().client_secret_entry.text().as_str().into(),
                }));
            }
        }))
    }

    pub fn connect_submit<F: Fn(Option<&ApiSecret>) + 'static>(&self, f: F) {
        self.imp()
            .submit_button
            .connect_clicked(clone!(@weak self as this => @default-panic, move |_button| {
                f(this.imp().api_secret.borrow().as_ref());
            }));
    }

    pub fn connect_ignore<F: Fn() + 'static>(&self, f: F) {
        self.imp().ignore_button.connect_clicked(move |_button| f());
    }

    pub fn connect_back<F: Fn(Option<&LoginPrevPage>) + 'static>(&self, f: F) {
        // setup back button to turn to previous page
        self.imp()
            .back_button
            .connect_clicked(clone!(@weak self as this => @default-panic, move |_button| {
                f(this.imp().prev_page.borrow().as_ref());
            }));
    }
}
