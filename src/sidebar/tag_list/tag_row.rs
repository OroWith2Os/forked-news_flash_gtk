use crate::app::App;
use crate::sidebar::tag_list::models::TagGObject;
use crate::util::{constants, GtkUtil};
use gio::{Menu, MenuItem};
use glib::{clone, subclass::*, ParamSpec, ParamSpecString, SignalHandlerId, Value};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, DrawingArea, Label};
use gtk4::{GestureClick, GestureLongPress, PopoverMenu, PositionType};
use news_flash::models::{PluginCapabilities, TagID};
use once_cell::sync::Lazy;
use std::cell::RefCell;
use std::rc::Rc;
use std::str;

static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
    vec![Signal::builder("activated")
        .param_types([TagRow::static_type()])
        .build()]
});

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/tag.ui")]
    pub struct TagRow {
        pub id: RefCell<TagID>,
        pub color: Rc<RefCell<String>>,
        pub popover: RefCell<Option<PopoverMenu>>,
        pub right_click_source: RefCell<Option<SignalHandlerId>>,
        pub long_press_source: RefCell<Option<SignalHandlerId>>,

        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<DrawingArea>,
        #[template_child]
        pub row_click: TemplateChild<GestureClick>,
        #[template_child]
        pub row_activate: TemplateChild<GestureClick>,
        #[template_child]
        pub row_long_press: TemplateChild<GestureLongPress>,
    }

    impl Default for TagRow {
        fn default() -> Self {
            TagRow {
                id: RefCell::new(TagID::new("")),
                color: Rc::new(RefCell::new(constants::TAG_DEFAULT_COLOR.into())),
                popover: RefCell::new(None),
                right_click_source: RefCell::new(None),
                long_press_source: RefCell::new(None),

                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
                row_click: TemplateChild::default(),
                row_activate: TemplateChild::default(),
                row_long_press: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TagRow {
        const NAME: &'static str = "TagRow";
        type ParentType = gtk4::Box;
        type Type = super::TagRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for TagRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("title").build(),
                    ParamSpecString::builder("color").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "title" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.tag_title.set_label(&input);
                }
                "color" => {
                    let input: String = value.get().expect("The value needs to be of type `string`.");
                    self.color.replace(input);
                    self.tag_color.queue_draw();
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "title" => self.obj().get_title().to_value(),
                "color" => self.color.borrow().clone().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for TagRow {}

    impl BoxImpl for TagRow {}
}

glib::wrapper! {
    pub struct TagRow(ObjectSubclass<imp::TagRow>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for TagRow {
    fn default() -> Self {
        let row = glib::Object::new::<Self>();
        row.init();
        row
    }
}

impl TagRow {
    pub fn new() -> Self {
        Self::default()
    }

    fn init(&self) {
        self.imp().row_activate.connect_released(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            this.activate();
        }));
    }

    pub fn bind_model(&self, model: &TagGObject) {
        let imp = self.imp();
        let is_same_tag = *imp.id.borrow() == model.tag_id();

        let support_mutation = App::default().features().contains(PluginCapabilities::SUPPORT_TAGS);

        imp.id.replace(model.tag_id());
        imp.color.replace(model.color());

        if support_mutation && imp.right_click_source.borrow().is_none() {
            self.setup_right_click();
        }

        imp.tag_color.set_draw_func(
            clone!(@strong imp.color as color => @default-panic, move |_drawing_area, ctx, _width, _height| {
                GtkUtil::draw_color_cirlce(ctx, &color.borrow(), None);
            }),
        );

        if !is_same_tag {
            imp.tag_title.set_label(&model.title());
        }
    }

    fn get_title(&self) -> String {
        self.imp().tag_title.text().as_str().to_string()
    }

    fn setup_right_click(&self) {
        let imp = self.imp();

        imp.right_click_source
            .replace(Some(imp.row_click.connect_released(clone!(
                @weak self as this => @default-panic, move |_gesture, times, _x, _y|
            {
                if times != 1 {
                    return
                }

                if App::default().content_page_state().borrow().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.borrow().is_none() {
                    imp.popover.replace(Some(this.setup_context_popover()));
                }

                if let Some(popover) = imp.popover.borrow().as_ref() {
                    popover.popup();
                };
            }))));

        imp.long_press_source
            .replace(Some(imp.row_long_press.connect_pressed(clone!(
                @weak self as this => @default-panic, move |_gesture, _x, _y|
            {
                if App::default().content_page_state().borrow().get_offline() {
                    return
                }

                let imp = this.imp();
                if imp.popover.borrow().is_none() {
                    imp.popover.replace(Some(this.setup_context_popover()));
                }

                if let Some(popover) = imp.popover.borrow().as_ref() {
                    popover.popup();
                };
            }))));
    }

    fn setup_context_popover(&self) -> PopoverMenu {
        let tag_id = self.imp().id.borrow().clone();

        let model = Menu::new();
        let rename_item = MenuItem::new(Some("Edit"), None);
        let delete_item = MenuItem::new(Some("Delete"), None);

        rename_item.set_action_and_target_value(Some("win.edit-tag-dialog"), Some(&tag_id.as_str().to_variant()));

        let tuple_variant = (tag_id.as_str().to_string(), self.get_title()).to_variant();
        delete_item.set_action_and_target_value(Some("win.enqueue-delete-tag"), Some(&tuple_variant));

        model.append_item(&rename_item);
        model.append_item(&delete_item);

        let popover = PopoverMenu::from_model(Some(&model));
        popover.set_parent(self);
        popover.set_position(PositionType::Bottom);
        popover
    }

    fn activate(&self) {
        self.emit_by_name::<()>("activated", &[&self.clone()]);
    }
}
