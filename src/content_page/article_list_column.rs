use super::article_list_mode::ArticleListMode;
use crate::app::App;
use crate::article_list::ArticleList;
use glib::{clone, subclass, Continue, SourceId};
use gtk4::{prelude::*, subclass::prelude::*, Button, CompositeTemplate, SearchBar, SearchEntry, Stack, ToggleButton};
use libadwaita::{HeaderBar, ViewStack};
use std::cell::RefCell;
use std::rc::Rc;
use std::time::Duration;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_list_column.ui")]
    pub struct ArticleListColumn {
        #[template_child]
        pub headerbar: TemplateChild<HeaderBar>,
        #[template_child]
        pub update_button: TemplateChild<Button>,
        #[template_child]
        pub update_stack: TemplateChild<Stack>,
        #[template_child]
        pub search_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub search_entry: TemplateChild<SearchEntry>,
        #[template_child]
        pub search_bar: TemplateChild<SearchBar>,
        #[template_child]
        pub mark_all_read_button: TemplateChild<Button>,
        #[template_child]
        pub mark_all_read_stack: TemplateChild<Stack>,
        #[template_child]
        pub show_sidebar_button: TemplateChild<ToggleButton>,
        #[template_child]
        pub view_switcher_stack: TemplateChild<ViewStack>,
        #[template_child]
        pub article_list: TemplateChild<ArticleList>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleListColumn {
        const NAME: &'static str = "ArticleListColumn";
        type ParentType = gtk4::Box;
        type Type = super::ArticleListColumn;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleListColumn {}

    impl WidgetImpl for ArticleListColumn {}

    impl BoxImpl for ArticleListColumn {}
}

glib::wrapper! {
    pub struct ArticleListColumn(ObjectSubclass<imp::ArticleListColumn>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ArticleListColumn {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleListColumn {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self) {
        let imp = self.imp();

        imp.update_button.connect_clicked(move |_button| {
            App::default().sync();
        });
        let mark_all_read_stack = imp.mark_all_read_stack.get();
        imp.mark_all_read_button.connect_clicked(move |button| {
            button.set_sensitive(false);
            mark_all_read_stack.set_visible_child_name("spinner");
            App::default().main_window().set_sidebar_read();
        });

        imp.article_list.init(&imp.search_bar);

        self.setup_view_switcher();
        Self::setup_search_button(&imp.search_button, &imp.search_bar);
        Self::setup_search_bar(&imp.search_bar, &imp.search_button, &imp.search_entry);
        Self::setup_search_entry(&imp.search_entry);
    }

    pub fn article_list(&self) -> &ArticleList {
        let imp = self.imp();
        &imp.article_list
    }

    pub fn headerbar(&self) -> &HeaderBar {
        let imp = self.imp();
        &imp.headerbar
    }

    pub fn show_sidebar_button(&self) -> &ToggleButton {
        let imp = self.imp();
        &imp.show_sidebar_button
    }

    pub fn update_stack(&self) -> &Stack {
        let imp = self.imp();
        &imp.update_stack
    }

    pub fn set_view_switcher_stack(&self, child_name: &str) {
        let imp = self.imp();
        imp.view_switcher_stack
            .set_visible_child_name(&format!("{}_placeholder", child_name));
    }

    fn setup_view_switcher(&self) {
        let imp = self.imp();

        let view_switcher_timeout: Rc<RefCell<Option<SourceId>>> = Rc::new(RefCell::new(None));
        let article_list_mode = Rc::new(RefCell::new(ArticleListMode::All));

        imp.view_switcher_stack.connect_visible_child_name_notify(clone!(
            @strong article_list_mode,
            @strong view_switcher_timeout,
            @strong self as column => @default-panic, move |stack|
        {
            if let Some(child_name) = stack.visible_child_name() {
                let child_name = child_name.to_string();

                article_list_mode.replace(if child_name.starts_with("unread") {
                    ArticleListMode::Unread
                } else if child_name.starts_with("marked") {
                    ArticleListMode::Marked
                } else {
                    ArticleListMode::All
                });

                if view_switcher_timeout.borrow_mut().is_some() {
                    return;
                }

                column.stack_switched(&article_list_mode, &view_switcher_timeout);
            }
        }));
    }

    fn stack_switched(
        &self,
        article_list_mode: &Rc<RefCell<ArticleListMode>>,
        view_switcher_timeout: &Rc<RefCell<Option<SourceId>>>,
    ) {
        self.set_mode((*article_list_mode.borrow()).clone());

        if view_switcher_timeout.borrow().is_some() {
            return;
        }

        let mode_before_cooldown = (*article_list_mode.borrow()).clone();
        view_switcher_timeout.replace(Some(glib::timeout_add_local(
            Duration::from_millis(250),
            clone!(
                @strong article_list_mode,
                @strong view_switcher_timeout,
                @strong self as column => @default-panic, move ||
            {
                view_switcher_timeout.take();
                if mode_before_cooldown != *article_list_mode.borrow() {
                    column.stack_switched(
                        &article_list_mode,
                        &view_switcher_timeout,
                    );
                }
                Continue(false)
            }),
        )));
    }

    fn set_mode(&self, new_mode: ArticleListMode) {
        let content_page_state = App::default().content_page_state();
        let old_selection = content_page_state.borrow().get_article_list_mode().clone();
        content_page_state.borrow_mut().set_article_list_mode(new_mode.clone());
        match new_mode {
            ArticleListMode::All => self.set_view_switcher_stack("all"),
            ArticleListMode::Unread => self.set_view_switcher_stack("unread"),
            ArticleListMode::Marked => self.set_view_switcher_stack("marked"),
        };
        App::default().update_article_list();

        let update_sidebar = match old_selection {
            ArticleListMode::All | ArticleListMode::Unread => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => false,
                ArticleListMode::Marked => true,
            },
            ArticleListMode::Marked => match new_mode {
                ArticleListMode::All | ArticleListMode::Unread => true,
                ArticleListMode::Marked => false,
            },
        };
        if update_sidebar {
            App::default().update_sidebar();
        }
    }

    fn setup_search_button(search_button: &ToggleButton, search_bar: &SearchBar) {
        search_button.connect_toggled(clone!(@weak search_bar => @default-panic, move |button| {
            if button.is_active() {
                search_bar.set_search_mode(true);
            } else {
                search_bar.set_search_mode(false);
            }
        }));
    }

    fn setup_search_bar(search_bar: &SearchBar, search_button: &ToggleButton, search_entry: &SearchEntry) {
        search_bar.connect_entry(search_entry);
        search_bar.connect_search_mode_enabled_notify(
            clone!(@weak search_button => @default-panic, move |search_bar| {
                if !search_bar.is_search_mode() {
                    search_button.set_active(false);
                }
            }),
        );
    }

    fn setup_search_entry(search_entry: &SearchEntry) {
        search_entry.connect_search_changed(move |search_entry| {
            App::default()
                .main_window()
                .set_search_term(search_entry.text().as_str().into());
        });
    }

    pub fn start_sync(&self) {
        let imp = self.imp();
        imp.update_stack.set_visible_child_name("spinner");
    }

    pub fn finish_sync(&self) {
        let imp = self.imp();
        imp.update_stack.set_visible_child_name("button");
    }

    pub fn finish_mark_all_read(&self) {
        let imp = self.imp();
        imp.mark_all_read_button.set_sensitive(true);
        imp.mark_all_read_stack.set_visible_child_name("button");
    }

    pub fn set_offline(&self, offline: bool) {
        self.imp().update_stack.set_sensitive(!offline);
    }

    pub fn is_search_focused(&self) -> bool {
        let imp = self.imp();
        imp.search_button.is_active() && imp.search_entry.has_focus()
    }

    pub fn focus_search(&self) {
        let imp = self.imp();
        // shortcuts ignored when focues -> no need to hide seach bar on keybind (ESC still works)
        imp.search_button.set_active(true);
        imp.search_entry.grab_focus();
    }
}
