use crate::app::App;
use crate::error::NewsFlashGtkError;
use crate::i18n::i18n;
use glib::subclass;
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CompositeTemplate, Stack};
use libadwaita::{Toast, ToastOverlay};
use news_flash::error::NewsFlashError;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/reset_page.ui")]
    pub struct ResetPage {
        #[template_child]
        pub reset_button: TemplateChild<Button>,
        #[template_child]
        pub back_button: TemplateChild<Button>,
        #[template_child]
        pub reset_stack: TemplateChild<Stack>,
        #[template_child]
        pub toast_overlay: TemplateChild<ToastOverlay>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ResetPage {
        const NAME: &'static str = "ResetPage";
        type ParentType = gtk4::Box;
        type Type = super::ResetPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ResetPage {}

    impl WidgetImpl for ResetPage {}

    impl BoxImpl for ResetPage {}
}

glib::wrapper! {
    pub struct ResetPage(ObjectSubclass<imp::ResetPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl Default for ResetPage {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ResetPage {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self) {
        let imp = self.imp();

        let reset_stack = imp.reset_stack.get();
        imp.reset_button.connect_clicked(move |button| {
            reset_stack.set_visible_child_name("reset_spinner");
            button.set_sensitive(false);
            App::default().reset_account();
        });

        imp.back_button.connect_clicked(move |_button| {
            App::default().main_window().cancel_reset();
        });
    }

    pub fn reset(&self) {
        let imp = self.imp();

        imp.reset_stack.set_visible_child_name("reset_label");
        imp.reset_button.set_sensitive(true);
    }

    pub fn error(&self, error: NewsFlashError) {
        self.reset();

        App::default().set_newsflash_error(NewsFlashGtkError::NewsFlash {
            source: error,
            context: "Error Resetting the account".into(),
        });

        let message = i18n("Failed to reset account");
        let toast = Toast::new(&message);
        toast.set_button_label(Some("details"));
        toast.set_action_name(Some("win.show-error-dialog"));
        self.imp().toast_overlay.add_toast(toast);
    }
}
