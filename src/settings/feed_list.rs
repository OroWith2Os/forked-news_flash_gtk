use glib::Enum;
use serde::{Deserialize, Serialize};
use std::{default::Default, fmt::Display};

use crate::i18n::i18n;

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub struct FeedListSettings {
    #[serde(default)]
    pub order: GFeedOrder,
    pub only_show_relevant: bool,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "GFeedOrder")]
pub enum GFeedOrder {
    Alphabetical,
    Manual,
}

impl Default for GFeedOrder {
    fn default() -> Self {
        Self::Manual
    }
}

impl Display for GFeedOrder {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            GFeedOrder::Alphabetical => write!(f, "{}", i18n("Alphabetical")),
            GFeedOrder::Manual => write!(f, "{}", i18n("Manual")),
        }
    }
}

impl From<u32> for GFeedOrder {
    fn from(value: u32) -> Self {
        match value {
            0 => Self::Alphabetical,
            1 => Self::Manual,
            _ => Self::Manual,
        }
    }
}

impl From<i32> for GFeedOrder {
    fn from(value: i32) -> Self {
        match value {
            0 => Self::Alphabetical,
            1 => Self::Manual,
            _ => Self::Manual,
        }
    }
}

impl From<GFeedOrder> for u32 {
    fn from(value: GFeedOrder) -> Self {
        match value {
            GFeedOrder::Alphabetical => 0,
            GFeedOrder::Manual => 1,
        }
    }
}

impl From<GFeedOrder> for i32 {
    fn from(value: GFeedOrder) -> Self {
        match value {
            GFeedOrder::Alphabetical => 0,
            GFeedOrder::Manual => 1,
        }
    }
}
