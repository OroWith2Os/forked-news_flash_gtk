mod models;
mod progress_overlay;
mod url_overlay;

pub use self::models::ArticleTheme;
use self::progress_overlay::ProgressOverlay;
use self::url_overlay::UrlOverlay;
use crate::app::{App, WEBKIT_DATA_DIR};
use crate::self_stack::SelfStack;
use crate::util::{constants, DateUtil, GtkUtil, Util, CHANNEL_ERROR, GTK_RESOURCE_FILE_ERROR};
use chrono::{DateTime, Duration, Utc};
use eyre::{eyre, Result};
use futures::channel::oneshot::Sender as OneShotSender;
use gdk4::{Key, ModifierType, ScrollDirection, ScrollEvent};
use gio::Cancellable;
use glib::Object;
use glib::{clone, source::Continue, subclass, MainLoop, SignalHandlerId, SourceId};
use gtk4::{
    prelude::*, subclass::prelude::*, Box, CompositeTemplate, EventControllerKey, EventControllerMotion,
    EventControllerScroll, EventSequenceState, GestureDrag, Inhibit, Overlay, Stack, TickCallbackId, Widget,
};
use log::{error, warn};
use news_flash::models::{Enclosure, FatArticle, Marked, Read, Url};
use pango::FontDescription;
use rust_embed::RustEmbed;
use std::cell::{Cell, RefCell};
use std::rc::Rc;
use std::str;
use url::{Host, Origin};
use webkit6::{
    prelude::*, CacheModel, ContextMenuAction, ContextMenuItem, NavigationPolicyDecision, NetworkProxyMode,
    NetworkProxySettings, NetworkSession, PolicyDecisionType, Settings as WebkitSettings, UserContentFilterStore,
    UserContentInjectedFrames, UserStyleLevel, UserStyleSheet, WebContext, WebView, WebsiteDataTypes,
};

#[derive(RustEmbed)]
#[folder = "data/resources/article_view"]
struct ArticleViewResources;

const SCROLL_TRANSITION_DURATION: i64 = 500 * 1000;

#[derive(Clone, Debug)]
pub struct ScrollAnimationProperties {
    pub start_time: Rc<RefCell<Option<i64>>>,
    pub end_time: Rc<RefCell<Option<i64>>>,
    pub scroll_callback_id: Rc<RefCell<Option<TickCallbackId>>>,
    pub transition_start_value: Rc<RefCell<Option<f64>>>,
    pub transition_diff: Rc<RefCell<Option<f64>>>,
}

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/article_view.ui")]
    pub struct ArticleView {
        #[template_child]
        pub progress_overlay: TemplateChild<Overlay>,
        #[template_child]
        pub url_overlay: TemplateChild<Overlay>,

        #[template_child]
        pub stack: TemplateChild<Stack>,
        #[template_child]
        pub self_stack: TemplateChild<SelfStack>,
        #[template_child]
        pub empty_status: TemplateChild<libadwaita::StatusPage>,

        pub view: RefCell<WebView>,
        pub web_context: RefCell<WebContext>,
        pub network_session: RefCell<NetworkSession>,

        #[template_child]
        pub scroll_event: TemplateChild<EventControllerScroll>,
        #[template_child]
        pub key_event: TemplateChild<EventControllerKey>,
        #[template_child]
        pub motion_event: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub drag_gesture: TemplateChild<GestureDrag>,

        #[template_child]
        pub url_overlay_label: TemplateChild<UrlOverlay>,
        #[template_child]
        pub progress_overlay_label: TemplateChild<ProgressOverlay>,

        pub visible_article: RefCell<Option<FatArticle>>,
        pub visible_feed_name: RefCell<Option<String>>,
        pub visible_article_enclosures: RefCell<Option<Vec<Enclosure>>>,
        pub visible_article_shows_scraped_content: Cell<bool>,

        pub load_start_timestamp: RefCell<DateTime<Utc>>,

        pub decide_policy_signal: RefCell<Option<SignalHandlerId>>,
        pub mouse_over_signal: RefCell<Option<SignalHandlerId>>,
        pub ctx_menu_signal: RefCell<Option<SignalHandlerId>>,
        pub load_signal: RefCell<Option<SignalHandlerId>>,

        pub drag_ongoing: Cell<bool>,
        pub drag_buffer: RefCell<[f64; 10]>,
        pub drag_y_offset: Cell<f64>,
        pub drag_momentum: Cell<f64>,
        pub pointer_pos: Cell<(f64, f64)>,
        pub current_scroll_pos: Cell<f64>,
        pub drag_buffer_update_signal: RefCell<Option<SourceId>>,
        pub drag_released_motion_signal: RefCell<Option<SourceId>>,
        pub scroll_animation_data: ScrollAnimationProperties,
    }

    impl Default for ArticleView {
        fn default() -> Self {
            let network_session = NetworkSession::new(WEBKIT_DATA_DIR.to_str(), WEBKIT_DATA_DIR.to_str());

            let proxies = App::default().settings().borrow().get_proxy();
            if !proxies.is_empty() {
                let mut proxy_settings = NetworkProxySettings::new(None, &[]);
                for proxy in proxies {
                    match proxy.protocoll {
                        crate::settings::ProxyProtocoll::All => {
                            proxy_settings.add_proxy_for_scheme("http", &proxy.url);
                            proxy_settings.add_proxy_for_scheme("https", &proxy.url);
                        }
                        crate::settings::ProxyProtocoll::Http => {
                            proxy_settings.add_proxy_for_scheme("http", &proxy.url)
                        }
                        crate::settings::ProxyProtocoll::Https => {
                            proxy_settings.add_proxy_for_scheme("https", &proxy.url)
                        }
                    }
                }
                network_session.set_proxy_settings(NetworkProxyMode::Custom, Some(&proxy_settings));
            }

            let web_context = WebContext::new();
            web_context.set_cache_model(CacheModel::DocumentBrowser);

            Self {
                progress_overlay: TemplateChild::default(),
                url_overlay: TemplateChild::default(),
                stack: TemplateChild::default(),
                self_stack: TemplateChild::default(),
                empty_status: TemplateChild::default(),

                load_start_timestamp: RefCell::new(Utc::now()),

                view: RefCell::new(super::ArticleView::new_webview(&web_context, &network_session)),
                web_context: RefCell::new(web_context),
                network_session: RefCell::new(network_session),

                scroll_event: TemplateChild::default(),
                key_event: TemplateChild::default(),
                motion_event: TemplateChild::default(),
                drag_gesture: TemplateChild::default(),

                url_overlay_label: TemplateChild::default(),
                progress_overlay_label: TemplateChild::default(),

                visible_article: RefCell::new(None),
                visible_feed_name: RefCell::new(None),
                visible_article_enclosures: RefCell::new(None),
                visible_article_shows_scraped_content: Cell::new(false),

                decide_policy_signal: RefCell::new(None),
                mouse_over_signal: RefCell::new(None),
                ctx_menu_signal: RefCell::new(None),
                load_signal: RefCell::new(None),

                drag_ongoing: Cell::new(false),
                drag_buffer: RefCell::new([0.0; 10]),
                drag_y_offset: Cell::new(0.0),
                drag_momentum: Cell::new(0.0),
                pointer_pos: Cell::new((0.0, 0.0)),
                current_scroll_pos: Cell::new(0.0),
                drag_buffer_update_signal: RefCell::new(None),
                drag_released_motion_signal: RefCell::new(None),
                scroll_animation_data: ScrollAnimationProperties {
                    start_time: Rc::new(RefCell::new(None)),
                    end_time: Rc::new(RefCell::new(None)),
                    scroll_callback_id: Rc::new(RefCell::new(None)),
                    transition_start_value: Rc::new(RefCell::new(None)),
                    transition_diff: Rc::new(RefCell::new(None)),
                },
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleView {
        const NAME: &'static str = "ArticleView";
        type ParentType = Box;
        type Type = super::ArticleView;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ArticleView {}

    impl WidgetImpl for ArticleView {}

    impl BoxImpl for ArticleView {}
}

glib::wrapper! {
    pub struct ArticleView(ObjectSubclass<imp::ArticleView>)
        @extends Widget, Box;
}

impl Default for ArticleView {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ArticleView {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn init(&self) {
        let imp = self.imp();

        imp.self_stack.set_transition_duration(150);
        imp.self_stack.set_widget(&*imp.view.borrow());

        self.connect_event_controller();
        self.connect_webview();

        imp.view.borrow().load_html("", None);
        imp.view.borrow().set_sensitive(false);
        imp.stack.set_visible_child_name("empty");
    }

    pub fn show_article(&self, article: FatArticle, feed_name: String, enclosures: Option<Vec<Enclosure>>) {
        let imp = self.imp();

        let is_new_article = if let Some(visible_article) = imp.visible_article.borrow().as_ref() {
            let visible_article_has_scraped_content = visible_article.scraped_content.is_some();
            let new_article_has_scraped_content = article.scraped_content.is_some();
            let scraped_content_visible = imp.visible_article_shows_scraped_content.get();

            visible_article.article_id != article.article_id
                || visible_article_has_scraped_content != new_article_has_scraped_content
                || scraped_content_visible
                    != App::default()
                        .content_page_state()
                        .borrow()
                        .get_prefer_scraped_content()
        } else {
            true
        };

        Self::stop_scroll_animation(&imp.view.borrow(), &imp.scroll_animation_data);
        Self::set_scroll_pos_static(&imp.view.borrow(), 0.0);

        imp.url_overlay_label.reveal(false);
        imp.progress_overlay_label.reveal(false);

        GtkUtil::remove_source(imp.drag_released_motion_signal.take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
        imp.drag_ongoing.set(false);

        if is_new_article {
            if !self.is_empty() {
                imp.self_stack.freeze();
            }

            imp.load_start_timestamp.replace(Utc::now());

            let html = self.build_article(&article, &feed_name);
            let base_url = Self::get_base_url(&article);

            imp.visible_article.replace(Some(article));
            imp.visible_feed_name.replace(Some(feed_name));
            imp.visible_article_enclosures.replace(enclosures);

            imp.view.borrow().load_html(&html, base_url.as_deref());
            imp.view.borrow().set_sensitive(true);

            if self.is_empty() {
                imp.stack.set_visible_child_name("article");
            } else {
                imp.self_stack.update(gtk4::StackTransitionType::Crossfade);
            }
        }

        self.grab_focus();
    }

    pub fn redraw_article(&self) {
        let imp = self.imp();

        if self.is_empty() || imp.visible_article.borrow().is_none() || imp.visible_feed_name.borrow().is_none() {
            warn!("Can't redraw article view. No article is on display.");
            return;
        }

        GtkUtil::remove_source(imp.drag_released_motion_signal.take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
        imp.drag_ongoing.set(false);

        if let Some(article) = imp.visible_article.borrow().as_ref() {
            if let Some(feed_name) = imp.visible_feed_name.borrow().as_deref() {
                imp.self_stack.freeze();
                imp.load_start_timestamp.replace(Utc::now());
                let html = self.build_article(article, feed_name);
                imp.view
                    .borrow()
                    .load_html(&html, Self::get_base_url(article).as_deref());
                imp.self_stack.update(gtk4::StackTransitionType::Crossfade);
                imp.view.borrow().set_sensitive(true);
            }
        }
    }

    fn is_empty(&self) -> bool {
        let imp = self.imp();
        imp.stack
            .visible_child_name()
            .map(|s| s.as_str() != "article")
            .unwrap_or(false)
    }

    fn get_base_url(article: &FatArticle) -> Option<String> {
        if let Some(url) = &article.url {
            match url.origin() {
                Origin::Opaque(_op) => None,
                Origin::Tuple(scheme, host, port) => {
                    let host = match host {
                        Host::Domain(domain) => domain,
                        Host::Ipv4(ipv4) => ipv4.to_string(),
                        Host::Ipv6(ipv6) => ipv6.to_string(),
                    };
                    Some(format!("{}://{}:{}", scheme, host, port))
                }
            }
        } else {
            None
        }
    }

    pub fn get_visible_article(&self) -> (Option<FatArticle>, Option<Vec<Enclosure>>) {
        let imp = self.imp();
        (
            (*imp.visible_article.borrow()).clone(),
            imp.visible_article_enclosures.borrow().clone(),
        )
    }

    pub fn update_visible_article(&self, read: Option<Read>, marked: Option<Marked>) {
        let imp = self.imp();
        if let Some(visible_article) = &mut *imp.visible_article.borrow_mut() {
            if let Some(marked) = marked {
                visible_article.marked = marked;
            }
            if let Some(read) = read {
                visible_article.unread = read;
            }
        }
    }

    pub fn close_article(&self) {
        let imp = self.imp();
        imp.view.borrow().set_sensitive(false);
        imp.view.borrow().load_html("", None);
        imp.url_overlay_label.reveal(false);
        imp.progress_overlay_label.reveal(false);
        imp.visible_article.take();
        imp.visible_feed_name.take();
        imp.stack.set_visible_child_name("empty");
    }

    pub fn update_background_color(&self) {
        if let Some(color) = GtkUtil::get_background_color(&App::default().main_window()) {
            if (color.alpha() - 1.0).abs() < 0.01 {
                self.imp().view.borrow().set_background_color(&color);
            }
            self.redraw_article();
        }
    }

    fn on_crash(&self) {
        let imp = self.imp();

        //hide overlays
        imp.progress_overlay_label.reveal(false);
        imp.url_overlay_label.reveal(false);

        GtkUtil::remove_source(imp.drag_released_motion_signal.take());
        GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
        imp.drag_ongoing.set(false);

        // disconnect webview signals
        self.disconnect_view_signals();

        // replace webview
        let new_view = Self::new_webview(&imp.web_context.borrow(), &imp.network_session.borrow());
        imp.self_stack.set_widget(&new_view);
        imp.view.replace(new_view);
        self.connect_webview();

        imp.stack.set_visible_child_name("crash");
    }

    fn disconnect_view_signals(&self) {
        let imp = self.imp();

        // disconnect signals
        GtkUtil::disconnect_signal(imp.decide_policy_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.mouse_over_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.ctx_menu_signal.take(), &*imp.view.borrow());
        GtkUtil::disconnect_signal(imp.load_signal.take(), &*imp.view.borrow());
    }

    fn new_webview(ctx: &WebContext, session: &NetworkSession) -> WebView {
        let settings = WebkitSettings::new();
        settings.set_enable_html5_database(false);
        settings.set_enable_html5_local_storage(false);
        settings.set_enable_page_cache(false);
        settings.set_enable_smooth_scrolling(true);
        settings.set_enable_javascript(true);
        settings.set_javascript_can_access_clipboard(false);
        settings.set_javascript_can_open_windows_automatically(false);
        settings.set_media_playback_requires_user_gesture(true);
        settings.set_user_agent_with_application_details(Some("NewsFlash"), None);
        settings.set_enable_developer_extras(App::default().settings().borrow().get_inspect_article_view());
        let auto_load = App::default().settings().borrow().get_article_view_load_images();
        settings.set_auto_load_images(auto_load);

        let webview = Object::builder::<WebView>()
            .property("network-session", session)
            .property("web-context", ctx)
            .build();
        webview.set_settings(&settings);
        Self::load_style_sheet(&webview);
        Self::load_content_filter(&webview);
        webview
    }

    fn load_content_filter(webview: &WebView) {
        if let Some(user_content_manager) = webview.user_content_manager() {
            if let Some(webkit_data_dir) = WEBKIT_DATA_DIR.to_str() {
                let store = UserContentFilterStore::new(webkit_data_dir);

                let json_data = ArticleViewResources::get("stylesheet_filter.json").expect(GTK_RESOURCE_FILE_ERROR);
                let cancellable: Option<&Cancellable> = None;
                store.save(
                    "stylesheetfilter",
                    &glib::Bytes::from_owned(json_data.data),
                    cancellable,
                    move |result| {
                        if let Ok(filter) = result {
                            user_content_manager.add_filter(&filter);
                            log::debug!("Stylesheet filter loaded");
                        } else if let Err(error) = result {
                            log::error!("Failed to load Stylesheet filter: {}", error);
                        }
                    },
                );
            }
        }
    }

    pub fn reload_style_sheet(&self) {
        let imp = self.imp();
        if let Some(user_content_manager) = imp.view.borrow().user_content_manager() {
            user_content_manager.remove_all_style_sheets();
        }

        Self::load_style_sheet(&imp.view.borrow());
    }

    pub fn load_style_sheet(webview: &WebView) {
        if let Some(user_content_manager) = webview.user_content_manager() {
            let content_width = App::default().settings().borrow().get_article_view_width();
            let line_height = App::default().settings().borrow().get_article_view_line_height();
            let stylesheet = Self::generate_user_style_sheet(content_width, line_height);
            user_content_manager.add_style_sheet(&stylesheet);
        }
    }

    pub fn generate_user_style_sheet(content_width: Option<u32>, line_height: Option<f32>) -> UserStyleSheet {
        let content_width = content_width.unwrap_or(constants::DEFAULT_ARTICLE_CONTENT_WIDTH);
        let line_height = line_height.unwrap_or(constants::DEFAULT_ARTICLE_LINE_HEIGHT);
        let css_data = ArticleViewResources::get("style.css").expect(GTK_RESOURCE_FILE_ERROR);
        let css_string: String = str::from_utf8(css_data.data.as_ref())
            .expect("Failed to load CSS from resources")
            .into();
        let css_string = css_string.replacen("$CONTENT_WIDTH", &format!("{}em", content_width), 1);
        let css_string = css_string.replacen("$LINE_HEIGHT", &format!("{}em", line_height), 1);

        UserStyleSheet::new(
            &css_string,
            UserContentInjectedFrames::AllFrames,
            UserStyleLevel::User,
            &[],
            &[],
        )
    }

    fn connect_event_controller(&self) {
        let imp = self.imp();

        imp.motion_event
            .connect_motion(clone!(@weak self as this => @default-panic, move |_controller, x, y| {
                let imp = this.imp();
                imp.pointer_pos.set((x, y));
            }));

        //----------------------------------
        // zoom with ctrl+scroll
        //----------------------------------
        imp.scroll_event.connect_scroll(
            clone!(@weak self as this => @default-panic, move |event_controller, _x_delta, y_delta| {
                let imp = this.imp();

                if let Some(event) = event_controller.current_event() {
                    if event_controller.current_event_state().contains(ModifierType::CONTROL_MASK) {
                        let zoom = imp.view.borrow().zoom_level();
                        if let Ok(scroll_event) = event.downcast::<ScrollEvent>() {
                            match scroll_event.direction() {
                                ScrollDirection::Up => imp.view.borrow().set_zoom_level(zoom + 0.25),
                                ScrollDirection::Down => imp.view.borrow().set_zoom_level(zoom - 0.25),
                                ScrollDirection::Smooth => {
                                    let diff = 10.0 * (y_delta);
                                    imp.view.borrow().set_zoom_level(zoom - diff);
                                }
                                _ => {}
                            }
                            return Inhibit(true);
                        }

                        return Inhibit(false);
                    }
                }
                Inhibit(false)
            }),
        );

        //------------------------------------------------
        // zoom with ctrl+PLUS/MINUS & reset with ctrl+0
        //------------------------------------------------
        imp.key_event.connect_key_pressed(
            clone!(@weak self as this => @default-panic, move |_controller, key, _keyval, state| {
                let imp = this.imp();

                if state.contains(ModifierType::CONTROL_MASK) {
                    let zoom = imp.view.borrow().zoom_level();
                    match key {
                        Key::KP_0 | Key::_0  => imp.view.borrow().set_zoom_level(1.0),
                        Key::KP_Add | Key::plus => imp.view.borrow().set_zoom_level(zoom + 0.25),
                        Key::KP_Subtract | Key::minus => imp.view.borrow().set_zoom_level(zoom - 0.25),
                        _ => return Inhibit(false),
                    }
                    return Inhibit(true);
                }
                Inhibit(false)
            }),
        );

        //------------------------------------------------
        // drag view with middle mouse button
        //------------------------------------------------
        imp.drag_gesture.connect_drag_begin(clone!(@weak self as this => @default-panic, move |gesture, _x, _y| {
            let imp = this.imp();

            gesture.set_state(EventSequenceState::Claimed);

            GtkUtil::remove_source(imp.drag_released_motion_signal.take());
            GtkUtil::remove_source(imp.drag_buffer_update_signal.take());
            Self::stop_scroll_animation(&imp.view.borrow(), &imp.scroll_animation_data);
            imp.drag_buffer.replace([0.0; 10]);
            imp.drag_ongoing.set(true);
            imp.drag_momentum.set(0.0);

            imp.current_scroll_pos.set(this.get_scroll_abs());

            imp.drag_buffer_update_signal.replace(
                Some(glib::timeout_add_local(std::time::Duration::from_millis(10), clone!(@weak this => @default-panic, move ||
                {
                    let imp = this.imp();

                    if !imp.drag_ongoing.get() {
                        imp.drag_buffer_update_signal.take();
                        return Continue(false);
                    }

                    for i in (1..10).rev() {
                        let value = (*imp.drag_buffer.borrow())[i - 1];
                        (*imp.drag_buffer.borrow_mut())[i] = value;
                    }

                    (*imp.drag_buffer.borrow_mut())[0] = imp.drag_y_offset.get();
                    imp.drag_momentum.set((*imp.drag_buffer.borrow())[9] - (*imp.drag_buffer.borrow())[0]);
                    Continue(true)
                })))
            );

        }));

        imp.drag_gesture.connect_drag_update(
            clone!(@weak self as this => @default-panic, move |gesture, _x, y_offset| {
                let imp = this.imp();

                gesture.set_state(EventSequenceState::Claimed);

                for i in (1..10).rev() {
                    let value = (*imp.drag_buffer.borrow())[i - 1];
                    (*imp.drag_buffer.borrow_mut())[i] = value;
                }

                (*imp.drag_buffer.borrow_mut())[0] = y_offset;
                imp.drag_momentum.set((*imp.drag_buffer.borrow())[9] - (*imp.drag_buffer.borrow())[0]);

                let scroll = imp.drag_y_offset.get() - y_offset;
                let scroll = scroll / imp.view.borrow().zoom_level();
                imp.drag_y_offset.set(y_offset);
                let scroll_pos = imp.current_scroll_pos.get();
                let new_scroll_pos = scroll_pos + scroll;
                imp.current_scroll_pos.set(new_scroll_pos);
                this.set_scroll_abs(new_scroll_pos);
            }),
        );

        imp.drag_gesture.connect_drag_end(clone!(@weak self as this => @default-panic, move |gesture, _x, _y| {
            let imp = this.imp();
            imp.drag_ongoing.set(false);
            imp.drag_y_offset.set(0.0);

            gesture.set_state(EventSequenceState::Claimed);

            let scroll_pos = Rc::new(Cell::new(this.get_scroll_abs()));
            let scroll_upper = this.get_scroll_upper();

            imp.drag_released_motion_signal.replace(
                Some(glib::timeout_add_local(std::time::Duration::from_millis(20), clone!(@weak this, @strong scroll_pos => @default-panic, move ||
                {
                    let imp = this.imp();

                    imp.drag_momentum.set(imp.drag_momentum.get() / 1.2);
                    let allocation = imp.view.borrow().allocation();

                    let page_size = f64::from(imp.view.borrow().allocated_height());
                    let adjust_value = page_size * imp.drag_momentum.get() / f64::from(allocation.height());
                    let adjust_value = adjust_value / imp.view.borrow().zoom_level();
                    let old_adjust = scroll_pos.get();
                    let upper = scroll_upper * imp.view.borrow().zoom_level();

                    if (old_adjust + adjust_value) > (upper - page_size)
                        || (old_adjust + adjust_value) < 0.0
                    {
                        imp.drag_momentum.set(0.0);
                    }

                    let new_scroll_pos = f64::min(old_adjust + adjust_value, upper - page_size);
                    scroll_pos.set(new_scroll_pos);
                    this.set_scroll_abs(new_scroll_pos);

                    if imp.drag_momentum.get().abs() < 1.0 || imp.drag_ongoing.get() {
                        imp.drag_released_motion_signal.take();
                        return Continue(false);
                    }

                    Continue(true)
                })))
            );
        }));
    }

    fn connect_webview(&self) {
        let imp = self.imp();

        //----------------------------------
        // open link in external browser
        //----------------------------------
        imp.decide_policy_signal
            .replace(Some(imp.view.borrow().connect_decide_policy(
                move |_closure_webivew, decision, decision_type| {
                    if decision_type == PolicyDecisionType::NewWindowAction {
                        let navigation_action = decision
                            .downcast_ref::<NavigationPolicyDecision>()
                            .and_then(|descision| descision.navigation_action());

                        if let Some(mut navigation_action) = navigation_action {
                            let is_blank = navigation_action
                                .frame_name()
                                .map(|name| name == "_blank")
                                .unwrap_or(false);
                            if is_blank {
                                if let Some(uri) = navigation_action
                                    .request()
                                    .and_then(|request| request.uri())
                                    .and_then(|uri| Url::parse(uri.as_str()).ok())
                                {
                                    App::default().open_url_in_default_browser(&uri, false);
                                }
                            }
                        }
                        decision.ignore();
                        return true;
                    } else if decision_type == PolicyDecisionType::NavigationAction {
                        let mut action = decision
                            .downcast_ref::<NavigationPolicyDecision>()
                            .and_then(|navigation_decision| navigation_decision.navigation_action());
                        let is_user_gesture = action.as_mut().map(|action| action.is_user_gesture()).unwrap_or(false);

                        if is_user_gesture {
                            if let Some(uri) = action
                                .as_mut()
                                .and_then(|action| action.request())
                                .and_then(|request| request.uri())
                                .and_then(|uri| Url::parse(uri.as_str()).ok())
                            {
                                decision.ignore();
                                App::default().open_url_in_default_browser(&uri, false);
                            }
                        }
                    }
                    false
                },
            )));

        //----------------------------------
        // show url overlay
        //----------------------------------
        imp.mouse_over_signal
            .replace(Some(imp.view.borrow().connect_mouse_target_changed(clone!(
                @weak self as this => @default-panic, move |_closure_webivew, hit_test, _modifiers|
            {
                let imp = this.imp();

                if hit_test.context_is_link() {
                    if let Some(uri) = hit_test.link_uri() {
                        let allocation = imp.stack.allocation();
                        let rel_x = imp.pointer_pos.get().0 / f64::from(allocation.width());
                        let rel_y = imp.pointer_pos.get().1 / f64::from(allocation.height());

                        let align = if rel_x <= 0.5 && rel_y >= 0.85 {
                            gtk4::Align::End
                        } else {
                            gtk4::Align::Start
                        };

                        imp.url_overlay_label.set_url(uri.as_str(), align);
                        imp.url_overlay_label.reveal(true);
                    }
                } else {
                    imp.url_overlay_label.reveal(false);
                }
            }))));

        //----------------------------------
        // clean up context menu
        //----------------------------------
        imp.ctx_menu_signal.replace(Some(imp.view.borrow().connect_context_menu(
            |_closure_webivew, ctx_menu, hit_test| {
                let menu_items = ctx_menu.items();

                for item in menu_items {
                    if item.is_separator() {
                        ctx_menu.remove(&item);
                        continue;
                    }

                    let keep_stock_actions = vec![
                        ContextMenuAction::CopyLinkToClipboard,
                        ContextMenuAction::Copy,
                        ContextMenuAction::CopyImageToClipboard,
                        ContextMenuAction::CopyImageUrlToClipboard,
                        ContextMenuAction::InspectElement,
                    ];

                    if !keep_stock_actions.contains(&item.stock_action()) {
                        ctx_menu.remove(&item);
                    }
                }

                if hit_test.context_is_image() {
                    if let Some(save_image_action) = App::default().main_window().lookup_action("save-webview-image") {
                        let image_uri = hit_test.image_uri().unwrap().as_str().to_variant();
                        let save_image_item =
                            ContextMenuItem::from_gaction(&save_image_action, "Save Image", Some(&image_uri));
                        ctx_menu.append(&save_image_item);
                    }
                }

                if hit_test.context_is_link() {
                    if let Some(open_uri_action) = App::default().main_window().lookup_action("open-uri-in-browser") {
                        let uri = hit_test.link_uri().unwrap().as_str().to_variant();
                        let open_uri_item = ContextMenuItem::from_gaction(&open_uri_action, "Open Link", Some(&uri));
                        ctx_menu.insert(&open_uri_item, 0);
                    }
                }

                if ctx_menu.first().is_none() {
                    return true;
                }

                false
            },
        )));

        //----------------------------------
        // display load progress
        //----------------------------------
        imp.load_signal
            .replace(Some(imp.view.borrow().connect_estimated_load_progress_notify(
                clone!(@weak self as this => @default-panic, move |closure_webivew| {
                    let imp = this.imp();

                    if Utc::now() < *imp.load_start_timestamp.borrow() + Duration::milliseconds(1500) {
                        imp.progress_overlay_label.reveal(false);
                        return;
                    }

                    let progress = closure_webivew.estimated_load_progress();
                    if (progress - 1.0).abs() < 0.01 {
                        imp.progress_overlay_label.reveal(false);
                        return;
                    }
                    imp.progress_overlay_label.reveal(true);
                    imp.progress_overlay_label.set_percentage(progress);
                }),
            )));

        //----------------------------------
        // crash view
        //----------------------------------
        imp.view.borrow().connect_web_process_terminated(
            clone!(@weak self as this => @default-panic, move |_closure_webivew, _reason|
            {
                this.on_crash();
            }),
        );

        //----------------------------------
        // fullscreen
        //----------------------------------
        imp.view.borrow().connect_enter_fullscreen(|_| {
            log::info!("video enter fullscreen");
            let main_window = App::default().main_window();

            main_window
                .content_page()
                .state()
                .borrow_mut()
                .set_video_fullscreen(true);

            if main_window.is_fullscreened() {
                main_window.notify("fullscreened");
            }
            false
        });
        imp.view.borrow().connect_leave_fullscreen(|_| {
            log::info!("video leave fullscreen");
            let main_window = App::default().main_window();

            main_window
                .content_page()
                .state()
                .borrow_mut()
                .set_video_fullscreen(false);

            if !main_window.is_fullscreened() {
                main_window.notify("fullscreened");
            }
            false
        });
    }

    fn build_article(&self, article: &FatArticle, feed_name: &str) -> String {
        let imp = self.imp();

        let prefer_dark_theme = libadwaita::StyleManager::default().is_dark();

        imp.visible_article_shows_scraped_content.set(
            App::default()
                .content_page_state()
                .borrow()
                .get_prefer_scraped_content(),
        );

        Self::build_article_static(
            article,
            feed_name,
            None,
            App::default()
                .content_page_state()
                .borrow()
                .get_prefer_scraped_content(),
            prefer_dark_theme,
        )
    }

    pub fn build_article_static(
        article: &FatArticle,
        feed_name: &str,
        selectable_override: Option<bool>,
        prefer_scraped_content: bool,
        prefer_dark_theme: bool,
    ) -> String {
        let settings = App::default().settings();
        let template_data = ArticleViewResources::get(&format!("{}.html", "article")).expect(GTK_RESOURCE_FILE_ERROR);
        let template_str = str::from_utf8(template_data.data.as_ref()).expect(GTK_RESOURCE_FILE_ERROR);
        let mut template_string = template_str.to_owned();

        // A list of fonts we should try to use in order of preference
        // We will pass all of these to CSS in order
        let mut font_options: Vec<String> = Vec::new();
        let mut font_families: Vec<String> = Vec::new();
        let mut font_size: Option<i32> = None;

        // Try to use the configured font if it exists
        if let Some(font_setting) = settings.borrow().get_article_view_font() {
            font_options.push(font_setting.into());
        }

        // If there is no configured font, or it's broken, use the system default font
        let font_system = App::default().desktop_settings().document_font();
        font_options.push(font_system);

        // Backup if the system font is broken too
        font_options.push("sans".to_owned());

        for font in font_options {
            let desc = FontDescription::from_string(&font);
            if let Some(family) = desc.family() {
                font_families.push(family.to_string());
            }
            if font_size.is_none() && desc.size() > 0 {
                font_size = Some(desc.size());
            }
        }

        // if font size configured use it, otherwise use 12 as default
        let font_size = font_size.unwrap_or(12);

        let font_size = font_size / pango::SCALE;
        let font_family = font_families.join(", ");

        let mut author_date = String::new();
        let date = DateUtil::format(&article.date);
        if let Some(author) = &article.author {
            author_date.push_str(&format!("posted by: {}, {}", author, date));
        } else {
            author_date.push_str(&date);
        }

        // $HTML
        if prefer_scraped_content {
            if let Some(html) = &article.scraped_content {
                template_string = template_string.replacen("$HTML", html, 1);
            } else if let Some(html) = &article.html {
                template_string = template_string.replacen("$HTML", html, 1);
            } else {
                template_string = template_string.replacen("$HTML", constants::NO_CONTENT, 1);
            }
        } else if let Some(html) = &article.html {
            template_string = template_string.replacen("$HTML", html, 1);
        } else {
            template_string = template_string.replacen("$HTML", constants::NO_CONTENT, 1);
        }

        // $UNSELECTABLE
        if selectable_override.unwrap_or(settings.borrow().get_article_view_allow_select()) {
            template_string = template_string.replacen("$UNSELECTABLE", "", 2);
        } else {
            template_string = template_string.replacen("$UNSELECTABLE", "unselectable", 2);
        }

        // $AUTHOR / $DATE
        template_string = template_string.replacen("$AUTHOR", &author_date, 1);

        // $SMALLSIZE x2
        let small_size = font_size - 2;
        template_string = template_string.replacen("$SMALLSIZE", &format!("{}", small_size), 2);

        // $TITLE (and URL)
        let mut title = article.title.as_deref().unwrap_or(constants::NO_TITLE).to_owned();
        if let Some(article_url) = &article.url {
            title = format!("<a href=\"{article_url}\" target=\"_blank\">{title}</a>")
        }
        template_string = template_string.replacen("$TITLE", &title, 1);

        // $LARGESIZE
        let large_size = font_size * 2;
        template_string = template_string.replacen("$LARGESIZE", &format!("{}", large_size), 1);

        // $FEED
        template_string = template_string.replacen("$FEED", feed_name, 1);

        // $THEME
        let theme = settings
            .borrow()
            .get_article_view_theme()
            .as_str(prefer_dark_theme)
            .to_owned();
        template_string = template_string.replacen("$THEME", &theme, 1);

        // $FONTFAMILY
        template_string = template_string.replacen("$FONTFAMILY", &font_family, 1);

        // $FONTSIZE
        template_string = template_string.replacen("$FONTSIZE", &format!("{}", font_size), 1);

        // hightlight.js
        let hightlight_js_data =
            ArticleViewResources::get("highlight.js/highlight.min.js").expect(GTK_RESOURCE_FILE_ERROR);
        let hightlight_js_string: String = str::from_utf8(hightlight_js_data.data.as_ref())
            .expect("Failed to load hightlight.js code from resources")
            .into();

        let hightlight_css_data =
            ArticleViewResources::get("highlight.js/dark.min.css").expect(GTK_RESOURCE_FILE_ERROR);
        let hightlight_css_string: String = str::from_utf8(hightlight_css_data.data.as_ref())
            .expect("Failed to load hightlight.js css from resources")
            .into();

        let hightlight_string = format!(
            r#"
            <style>
                {hightlight_css_string}
            </style>
            <script>
                {hightlight_js_string}
            </script>
        "#
        );

        template_string = template_string.replacen("$HIGHTLIGHT", &hightlight_string, 1);

        let mut mathjax_string = None;

        if let Some(feed_settings) = settings.borrow().get_feed_settings(&article.feed_id) {
            // mathjax
            let inline_math = feed_settings
                .inline_math
                .as_ref()
                .map(|inline| format!("inlineMath: [['{}', '{}']]", inline, inline))
                .unwrap_or_default();
            let mathjax_data = ArticleViewResources::get("mathjax/tex-svg.js").expect(GTK_RESOURCE_FILE_ERROR);
            let mathjax_js_str =
                str::from_utf8(mathjax_data.data.as_ref()).expect("Failed to load MATHJAX from resources");
            mathjax_string = Some(format!(
                r#"
                <script>
                    MathJax = {{
                        tex: {{{inline_math}}},
                        svg: {{fontCache: 'global'}}
                    }};
                </script>
                <script id="MathJax-script" async>
                    {mathjax_js_str}
                </script>
            "#
            ));
        }

        template_string = template_string.replacen("$MATHJAX", mathjax_string.as_deref().unwrap_or_default(), 1);

        template_string
    }

    fn set_scroll_pos_static(view: &WebView, pos: f64) {
        let cancellable: Option<&Cancellable> = None;
        view.evaluate_javascript(
            &format!("window.scrollTo(0,{});", pos),
            None,
            None,
            cancellable,
            |res| match res {
                Ok(_) => {}
                Err(_) => error!("Setting scroll pos failed"),
            },
        );
    }

    fn get_scroll_pos_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(view, "window.scrollY").expect("Failed to get scroll position from webview.")
    }

    fn get_scroll_window_height_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(view, "window.innerHeight").expect("Failed to get window height from webview.")
    }

    fn get_scroll_upper_static(view: &WebView) -> f64 {
        Self::webview_js_get_f64(
            view,
            "Math.max (
            document.body.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.clientHeight,
            document.documentElement.scrollHeight,
            document.documentElement.offsetHeight
        )",
        )
        .expect("Failed to get upper limit from webview.")
    }

    fn webview_js_get_f64(view: &WebView, java_script: &str) -> Result<f64> {
        let wait_loop = Rc::new(MainLoop::new(None, false));
        let value: Rc<RefCell<Option<f64>>> = Rc::new(RefCell::new(None));
        let cancellable: Option<&Cancellable> = None;
        view.evaluate_javascript(
            java_script,
            None,
            None,
            cancellable,
            clone!(@weak wait_loop, @weak value => @default-panic, move |res| {
                match res {
                    Ok(result) => {
                        value.replace(Some(result.to_double()));
                    }
                    Err(_) => error!("Getting scroll pos failed"),
                }
                wait_loop.quit();
            }),
        );

        wait_loop.run();

        let output = if let Some(pos) = value.take() {
            Ok(pos)
        } else {
            Err(eyre!("No value from JS"))
        };

        output
    }

    fn set_scroll_abs(&self, scroll: f64) {
        let imp = self.imp();
        Self::set_scroll_pos_static(&imp.view.borrow(), scroll);
    }

    fn get_scroll_abs(&self) -> f64 {
        let imp = self.imp();
        Self::get_scroll_pos_static(&imp.view.borrow())
    }

    fn get_scroll_window_height(&self) -> f64 {
        let imp = self.imp();
        Self::get_scroll_window_height_static(&imp.view.borrow())
    }

    fn get_scroll_upper(&self) -> f64 {
        let imp = self.imp();
        Self::get_scroll_upper_static(&imp.view.borrow())
    }

    pub fn animate_scroll_diff(&self, diff: f64) {
        let pos = self.get_scroll_abs();
        let upper = self.get_scroll_upper();
        let window_height = self.get_scroll_window_height();

        if (pos <= 0.0 && diff.is_sign_negative()) || (pos >= (upper - window_height) && diff.is_sign_positive()) {
            return;
        }

        self.animate_scroll_absolute(pos + diff, pos);
    }

    fn animate_scroll_absolute(&self, target: f64, current_pos: f64) {
        let animate = match gtk4::Settings::default() {
            Some(settings) => settings.is_gtk_enable_animations(),
            None => false,
        };

        if !self.is_mapped() || !animate {
            self.set_scroll_abs(target);
            return;
        }

        let imp = self.imp();

        imp.scroll_animation_data
            .start_time
            .replace(self.frame_clock().map(|clock| clock.frame_time()));
        imp.scroll_animation_data.end_time.replace(
            self.frame_clock()
                .map(|clock| clock.frame_time() + SCROLL_TRANSITION_DURATION),
        );

        let callback_id = imp.scroll_animation_data.scroll_callback_id.take();
        let leftover_scroll = match callback_id {
            Some(callback_id) => {
                callback_id.remove();
                let start_value =
                    Util::some_or_default(*imp.scroll_animation_data.transition_start_value.borrow(), 0.0);
                let diff_value = Util::some_or_default(*imp.scroll_animation_data.transition_diff.borrow(), 0.0);
                start_value + diff_value - current_pos
            }
            None => 0.0,
        };

        let diff = if (target + 1.0).abs() < 0.001 {
            -current_pos
        } else {
            (target - current_pos) + leftover_scroll
        };

        imp.scroll_animation_data.transition_diff.replace(Some(diff));

        imp.scroll_animation_data
            .transition_start_value
            .replace(Some(current_pos));

        imp.scroll_animation_data
            .scroll_callback_id
            .replace(Some(imp.view.borrow().add_tick_callback(clone!(
                @strong imp.scroll_animation_data as scroll_animation_data => @default-panic, move |view, clock|
            {
                let start_value = Util::some_or_default(*scroll_animation_data.transition_start_value.borrow(), 0.0);
                let diff_value = Util::some_or_default(*scroll_animation_data.transition_diff.borrow(), 0.0);
                let now = clock.frame_time();
                let end_time_value = Util::some_or_default(*scroll_animation_data.end_time.borrow(), 0);
                let start_time_value = Util::some_or_default(*scroll_animation_data.start_time.borrow(), 0);

                if !view.is_mapped() {
                    Self::set_scroll_pos_static(view, start_value + diff_value);
                    return Continue(false);
                }

                if scroll_animation_data.end_time.borrow().is_none() {
                    return Continue(false);
                }

                let t = if now < end_time_value {
                    (now - start_time_value) as f64 / (end_time_value - start_time_value) as f64
                } else {
                    1.0
                };

                let t = Util::ease_out_cubic(t);

                Self::set_scroll_pos_static(view, start_value + (t * diff_value));

                let pos = Self::get_scroll_pos_static(view);
                let upper = Self::get_scroll_upper_static(view);
                let window_height = Self::get_scroll_window_height_static(view);

                if (pos <= 0.0 && diff_value.is_sign_negative())
                || (pos + window_height >= upper && diff_value.is_sign_positive())
                || now >= end_time_value {
                    Self::stop_scroll_animation(view, &scroll_animation_data);
                    return Continue(false);
                }

                Continue(true)
            }))));
    }

    fn stop_scroll_animation(view: &WebView, properties: &ScrollAnimationProperties) {
        if let Some(callback_id) = properties.scroll_callback_id.take() {
            callback_id.remove();
        }
        view.queue_draw();
        properties.transition_start_value.take();
        properties.transition_diff.take();
        properties.start_time.take();
        properties.end_time.take();
    }

    pub fn clear_cache(&self, oneshot_sender: OneShotSender<()>) {
        let imp = self.imp();
        let data_manager = imp
            .view
            .borrow()
            .network_session()
            .and_then(|session| session.website_data_manager());

        if let Some(data_manager) = data_manager {
            let cancellable: Option<&Cancellable> = None;
            data_manager.clear(
                WebsiteDataTypes::all(),
                glib::TimeSpan::from_seconds(0),
                cancellable,
                move |res| {
                    if let Err(error) = res {
                        log::error!("Failed to clear webkit cache: {}", error);
                    }
                    oneshot_sender.send(()).expect(CHANNEL_ERROR);
                },
            );
        }
    }
}
